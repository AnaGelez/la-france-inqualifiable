use elefren::{helpers::cli, prelude::*, status_builder::Visibility};
use rand::seq::SliceRandom;
use rand::thread_rng;

fn main() -> Result<(), elefren::errors::Error> {
	let registration = Registration::new("https://botsin.space")
	    .client_name("La France Inqualifiable")
	    .scopes(Scopes::all())
	    .build()?;
	let mastodon = cli::authenticate(registration)?;

	let mut rng = thread_rng();
	let mut words: Vec<_> = include_str!("../adjectifs.txt").lines().collect();
	words.shuffle(&mut rng);
	for word in words {
		mastodon.new_status(StatusBuilder::new()
			.status(format!("La France {}", word))
			.visibility(Visibility::Unlisted)
			.build()?
		)?;

		std::thread::sleep(std::time::Duration::from_millis(1000 * 60 * 60 * 3));
	}

	Ok(())
}
